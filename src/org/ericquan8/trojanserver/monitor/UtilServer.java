/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.ericquan8.trojanserver.monitor;

/**
 * 
 * @author eric
 */
public class UtilServer {

	public static boolean checkIp(String ip) {
		if (ip == null) {
			return false;
		}
		String[] dps = ip.split(".");
		if (dps.length != 4 && dps.length != 6) {
			return false;
		}
		boolean isIp = true;
		for (int i = 0; i < dps.length; i++) {
			try {
				int dp = Integer.parseInt(dps[i]);
				if (dp > 255 || dp < 0) {
					throw new RuntimeException("error IP");
				}
			} catch (Exception e) {
				isIp = false;
				break;
			}
		}
		return isIp;
	}
}
